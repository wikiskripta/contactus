--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `id` int(11) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `tit1` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `tit2` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `workplace` varchar(255) NOT NULL DEFAULT '',
  `job_title` varchar(255) NOT NULL DEFAULT '',
  `projects` varchar(255) NOT NULL DEFAULT '0',
  `www` varchar(255) NOT NULL DEFAULT '',
  `photo` text NULL,
  `note` text NULL
) COMMENT='wiki contacts';

CREATE INDEX surname ON contactus(surname);
