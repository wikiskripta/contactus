<?php

/**
 * All hooked functions used by Contactus extension.
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class ContactusHooks {

	public static function efContactus_Setup( &$parser ) {
		$parser->setFunctionHook( 'contactus', 'ContactusHooks::efContactus_Render' );
		return true;
	}

	public static function efContactus_Render( &$parser, $id, $param2 = 350 ) {

		global $wgContactusFieldsCard, $wgContactusFieldsTable, $wgServer;
		$output = "";
		$parser->getOutput()->addModules( 'ext.Contactus.magic' );
		$arr = explode( ",", $id );	// může tam být více ID oddělených čárkou

		$conn = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancer();
		$dbr = $conn->getConnectionRef(DB_REPLICA);

		if( $param2 == "project" || $param2 == "projekt" ) {

			## PROJEKT ##
			$output .= "<div style='overflow-x:auto;'><table id='contactusList' class='wikitable sortable'><tr>";
			if( in_array( "photo", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'></th>";
			$output .= "<th class='unsortable'>" . wfMessage( 'contactus-name' )->text() . "</th>";
			if( in_array( "email", $wgContactusFieldsTable ) ) $output .= "<th>Email</th>";
			if( in_array( "phone", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'>" . wfMessage( 'contactus-phone' )->text() . "</th>";
			if( in_array( "workplace", $wgContactusFieldsTable ) ) $output .= "<th>" . wfMessage( 'contactus-workplace' )->text() . "</th>";
			if( in_array( "jobtitle", $wgContactusFieldsTable ) ) $output .= "<th>" . wfMessage( 'contactus-jobtitle' )->text() . "</th>";
			if( in_array( "www", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'>" . wfMessage( 'contactus-www' )->text() . "</th>";
			if( in_array( "note", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'>" . wfMessage( 'contactus-note' )->text() . "</th>";
			$output .= "</tr>";

			$res = $dbr->selectRow(
				'contactus_projects',
				array( 'id' ),
				array( 'id' => $id )
			);
			if(!$res) return true;	

			$res = $dbr->select(
				'contactus',
				array( 'tit1', 'name', 'surname', 'tit2', 'email', 'phone', 'workplace', 'job_title', 'www', 'note', 'photo', 'projects' ),
				array(),
				__METHOD__,
				array( 'ORDER BY' => 'surname' )
			);
			if(!$res) return true;

			foreach( $res as $row ) {
				if( preg_match( "/^".$id."$/", $row->projects ) || preg_match( "/^".$id.",/", $row->projects ) || preg_match( "/,".$id."$/", $row->projects ) || preg_match( "/,".$id.",/", $row->projects )) {	
					$output .= "<tr>";
					if( in_array( "photo", $wgContactusFieldsTable ) ) {
						$output .= "<td class='cphoto'>";
						if( !empty($row->photo) ) {
							$json = file_get_contents( "$wgServer/api.php?action=parse&contentmodel=wikitext&format=json&text=[[File:" . urlencode($row->photo) . "|40px]]" );
							$json = json_decode($json);
							$json = $json->parse->text->{"*"};
							$output .= $json;
						}
						$output .= "</td>";
					}

					$output .= "<td class='cname'>";
					if( in_array( "degree", $wgContactusFieldsTable ) && !empty($row->tit1) ) $output .= $row->tit1 . " ";
					$output .= $row->name . " " . $row->surname;
					if( in_array( "degree", $wgContactusFieldsTable ) && !empty($row->tit2) ) $output .= ", " . $row->tit2;
					$output .= "</td>";
					if( in_array( "email", $wgContactusFieldsTable ) ) {
						if( !empty($row->email) ) {
							$output .= "<td class='cemail'>". $row->email ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "phone", $wgContactusFieldsTable ) ) {
						if( !empty($row->phone) ) {
							$output .= "<td class='cphone'>". $row->phone ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "workplace", $wgContactusFieldsTable ) ) {
						if( !empty($row->workplace) ) {
							$output .= "<td class='cworkplace'>". $row->workplace ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "jobtitle", $wgContactusFieldsTable ) ) {
						if( !empty($row->job_title) ) {
							$output .= "<td class='cjobtitle'>". $row->job_title ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "www", $wgContactusFieldsTable ) ) {
						if( !empty($row->www) ) {
							if( strpos( $row->www, "http" ) !== false ) $url = $row->www; else $url = "http://" . $row->www;
							$output .= "<td class='cwww'><a href='$url'>www</a></td>";
						}
					}
					if( in_array( "note", $wgContactusFieldsTable ) ) {
						if( !empty($row->note) ) {
							$output .= "<td class='cnote'>". $row->note ."</td>";
						}
						else $output .= "<td></td>";
					}
					$output .= "</tr>";		
				}			
			}
			$output .= "<tr class='sortbottom'><td colspan='".sizeof($wgContactusFieldsTable)."'><a href='$wgServer/index.php?title=Special:Contactus'><span style='color:#8f999b;font-style:italic;'>" . wfMessage( 'contactus' )->text() . "</span></a></td></tr>";
			$output .= "</table></div>";
		}
		elseif( sizeof($arr) > 1 ) {
			## tabulka ##
			$output .= "<div style='overflow-x:auto;'><table id='contactusList' class='wikitable sortable'><tr>";
			if( in_array( "photo", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'></th>";
			$output .= "<th class='unsortable'>" . wfMessage( 'contactus-name' )->text() . "</th>";
			if( in_array( "email", $wgContactusFieldsTable ) ) $output .= "<th>Email</th>";
			if( in_array( "phone", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'>" . wfMessage( 'contactus-phone' )->text() . "</th>";
			if( in_array( "workplace", $wgContactusFieldsTable ) ) $output .= "<th>" . wfMessage( 'contactus-workplace' )->text() . "</th>";
			if( in_array( "jobtitle", $wgContactusFieldsTable ) ) $output .= "<th>" . wfMessage( 'contactus-jobtitle' )->text() . "</th>";
			if( in_array( "www", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'>" . wfMessage( 'contactus-www' )->text() . "</th>";
			if( in_array( "projects", $wgContactusFieldsTable ) ) $output .= "<th>" . wfMessage( 'contactus-projects' )->text() . "</th>";
			if( in_array( "note", $wgContactusFieldsTable ) ) $output .= "<th class='unsortable'>" . wfMessage( 'contactus-note' )->text() . "</th>";
			$output .= "</tr>";

			foreach( $arr as $uid ) {
				$uid = trim( $uid );
				if( $id > 0 && is_numeric($uid) ) {
					$res = $dbr->selectRow(
						'contactus',
						array( 'tit1', 'name', 'surname', 'tit2', 'email', 'phone', 'workplace', 'job_title', 'www', 'note', 'photo', 'projects' ),
						array( 'id' => $uid ),
						__METHOD__,
						array( 'ORDER BY' => 'surname' )
					);
					if(!$res) continue;

					$output .= "<tr>";
					if( in_array( "photo", $wgContactusFieldsTable ) ) {
						$output .= "<td class='cphoto'>";
						if( !empty($res->photo) ) {
							$json = file_get_contents( "$wgServer/api.php?action=parse&contentmodel=wikitext&format=json&text=[[File:" . urlencode($res->photo) . "|40px]]" );
							$json = json_decode($json);
							$json = $json->parse->text->{"*"};
							$output .= $json;
						}
						$output .= "</td>";
					}

					$output .= "<td class='cname'>";
					if( in_array( "degree", $wgContactusFieldsTable ) && !empty($res->tit1) ) $output .= $res->tit1 . " ";
					$output .= $res->name . " " . $res->surname;
					if( in_array( "degree", $wgContactusFieldsTable ) && !empty($res->tit2) ) $output .= ", " . $res->tit2;
					$output .= "</td>";
					if( in_array( "email", $wgContactusFieldsTable ) ) {
						if( !empty($res->email) ) {
							$output .= "<td class='cemail'>". $res->email ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "phone", $wgContactusFieldsTable ) ) {
						if( !empty($res->phone) ) {
							$output .= "<td class='cphone'>". $res->phone ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "workplace", $wgContactusFieldsTable ) ) {
						if( !empty($res->workplace) ) {
							$output .= "<td class='cworkplace'>". $res->workplace ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "jobtitle", $wgContactusFieldsTable ) ) {
						if( !empty($res->job_title) ) {
							$output .= "<td class='cjobtitle'>". $res->job_title ."</td>";
						}
						else $output .= "<td></td>";
					}
					if( in_array( "www", $wgContactusFieldsTable ) ) {
						if( !empty($res->www) ) {
							if( strpos( $res->www, "http" ) !== false ) $url = $res->www; else $url = "http://" . $res->www;
							$output .= "<td class='cwww'><a href='$url'>www</a></td>";
						}
					}

					if( in_array( "projects", $wgContactusFieldsTable ) ) {
						$output .= "<td class='cprojects'>";
						if( !empty($res->projects) ) {
							$arr = explode( ",", $res->projects );
							foreach( $arr as $pid ) {
								$res2 = $dbr->selectRow(
									'contactus_projects',
										array( 'id', 'name', 'place', 'www', 'isrecent' ),
										array( 'id' => $pid )
								);
								if( $res2 ) {
									$title = "";
									$recentstyle = 'color:red;';
									if( $res2->isrecent ) {
										$title .= wfMessage( 'contactus-isrecent' )->text();
										if( $res2->place ) $title .= ", ";
										$recentstyle = "color:green;";
									}
								}
								if( $res2->place ) $title .= $res2->place;
								$url = "";
								if( !preg_match("/http/", $res2->www ) ) $url = "http://" . $res2->www; else $url = $res2->www;
								$output .= "<a href='$url' title='$title' style='$recentstyle'>" . $res2->name . "</a><br>";
							}
						}
						$output .= "</td";
					}
					if( in_array( "note", $wgContactusFieldsTable ) ) {
						if( !empty($res->note) ) {
							$output .= "<td class='cnote'>". $res->note ."</td>";
						}
						else $output .= "<td></td>";
					}
					$output .= "</tr>";
				}
			}
			$output .= "<tr class='sortbottom'><td colspan='".sizeof($wgContactusFieldsTable)."'><a href='$wgServer/index.php?title=Special:Contactus'><span style='color:#8f999b;font-style:italic;'>" . wfMessage( 'contactus' )->text() . "</span></a></td></tr>";
			$output .= "</table></div>";
		}
		else {
			## vizitka ##
			$uid = trim( $arr[0] );
			$res = $dbr->selectRow(
				'contactus',
				array( 'tit1', 'name', 'surname', 'tit2', 'email', 'phone', 'workplace', 'job_title', 'www', 'note', 'photo', 'projects' ),
				array( 'id' => $id )
			);
			$output .= "<div class='contactus' style='padding:5px; margin:5px; border:1px solid #c4c3c2; width:$param2"."px;"."'>\n";
			if($res) {
				// photo
				if( in_array( "photo", $wgContactusFieldsCard ) && !empty($res->photo) ) {
					$json = file_get_contents( "$wgServer/api.php?action=parse&contentmodel=wikitext&format=json&text=[[File:" . urlencode($res->photo) . "|40px]]" );
					$json = json_decode($json);
					$json = $json->parse->text->{"*"};
					$output .= "<div style='float:left;width:50px;padding-right:10px;'>$json</div>\n";
				}
				$output .= "<div style='float:left;'>\n";
				// name, surname
				$output .= "<strong>";
				if( in_array( "degree", $wgContactusFieldsCard ) && !empty($res->tit1) ) $output .= $res->tit1 . " ";
				$output .= $res->name . " " . $res->surname;
				if( in_array( "degree", $wgContactusFieldsCard ) && !empty($res->tit2) ) $output .= ", " . $res->tit2;
				$output .= "</strong>";
				// workplace
				if( in_array( "workplace", $wgContactusFieldsCard ) && !empty($res->workplace) ) $output .= "<br>" . $res->workplace;
				// jobtitle
				if( in_array( "jobtitle", $wgContactusFieldsCard ) && !empty($res->job_title) ) $output .= "<br>" . $res->job_title;
				// email and phone
				if( in_array( "email", $wgContactusFieldsCard ) && !empty($res->email) ) {
					$output .= "<br/><strong>Email: </strong>";
					$output .= $res->email;
				}
				if( in_array( "phone", $wgContactusFieldsCard ) && !empty($res->phone) ) {
					$output .= "<br/><strong>" . wfMessage("contactus-phone")->text() . ": </strong>";
					$output .= $res->phone;
				}
				if( in_array( "www", $wgContactusFieldsCard ) && !empty($res->www) ) {
					if( strpos( $res->www, "http" ) !== false ) $url = $res->www; else $url = "http://" . $res->www;
					$output .= "<br/><a href='$url'>" . wfMessage("contactus-www")->text() . "</a>";
				}
				if( in_array( "note", $wgContactusFieldsCard ) && !empty($res->note) ) {
					$output .= "<br/><strong>" . wfMessage("contactus-note")->text() . ": </strong>";
					$output .= $res->note;
				}
				if( in_array( "projects", $wgContactusFieldsCard ) && !empty($res->projects) ) {
					$output .= "<br/><strong>" . wfMessage("contactus-projects")->text() . ": </strong>";
					$arr = explode( ",", $res->projects );
					foreach( $arr as $pid ) {
						$res2 = $dbr->selectRow(
							'contactus_projects',
								array( 'id', 'name', 'place', 'www', 'isrecent' ),
								array( 'id' => $pid )
						);
						if( $res2 ) {
							$title = "";
							$recentstyle = 'color:red;';
							if( $res2->isrecent ) {
								$title .= wfMessage( 'contactus-isrecent' )->text();
								if( $res2->place ) $title .= ", ";
								$recentstyle = "color:green;";
							}
						}
						if( $res2->place ) $title .= $res2->place;
						$url = "";
						if( !preg_match("/http/", $res2->www ) ) $url = "http://" . $res2->www; else $url = $res2->www;
						$output .= "<div title='$title' style='$recentstyle'><a href='$url'>" . $res2->name . "</a></div>";
					}
				}
				$output .= "</div>\n";
			}
			else {
				$output = wfMessage("contactus-error-magic")->text();
			}
			$output .= "<div style='clear:both;'></div>";
			$output .= "<div style='text-align:right;'><a href='$wgServer/index.php?title=Special:Contactus'><span style='color:#8f999b;font-style:italic;'>" . wfMessage( 'contactus' )->text() . "</span></a></div>";
			$output .= "</div>\n";
		}

		return $parser->insertStripItem( $output, $parser->mStripState );
	}

	# create or upgrade new tables
	public static function ContactusUpdateSchema( DatabaseUpdater $updater ) {

		if( $updater->getDB()->getType() == 'mysql' ) {
			$updater->addExtensionUpdate( array( 'addTable', 'contactus', __DIR__ . '/contactus.sql', true ) );
			$updater->addExtensionUpdate( array( 'addTable', 'contactus_projects', __DIR__ . '/contactus_projects.sql', true ) );
			return true;
		}
		else {
			return false;
		}
	}
}