<?php

/**
 * Magic words for extension.
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 * @file
 */

$magicWords = array();

/** English (English) */
$magicWords['en'] = array(
	'contactus' => array( 0, 'contactus' ),
);
