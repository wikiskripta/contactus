# Contactus

Mediawiki extension.

## Description

* Version 1.1.1
* _Contactus_ can insert/edit/remove contacts and projects.
* Aimed for projects' wikies.
* Contacts' database managed from special page
* Contacts can be inserted by magic word `{{#contactus:id1,id2, ... |width_in_px}}`

```php
{{#contactus:ID1,ID2,ID3}} ... table of contacts
{{#contactus:ID1|Width_in_pixels}} ... visit card
{{#contactus:ID1|project}} ... table with contacts related to the project
```

## Installation

* Make sure you have MediaWiki 1.36+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'Contactus' )`;
* Run `maintenance/update.php`, "contactus" and "contactus_projects" tables will be added.
* Check config in _extension.json_. Columns of user detail can be removed or added. These are the complete default options:

```php
"ContactusFieldsSpecial": ["degree", "photo", "email", "phone", "www", "workplace", "jobtitle", "note"],
"ContactusFieldsCard": ["degree", "photo", "email", "phone", "www", "workplace", "jobtitle", "note"],
"ContactusFieldsTable": ["degree", "photo", "email", "phone", "www", "workplace", "jobtitle", "note"]
```

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Special Page

* _Special:Contactus_: editing for sysops, bureaucrats only

## Release Notes

### 1.1.1

* Fixing "The constant DB_SLAVE/MASTER deprecated in 1.28. Use DB_REPLICA/PRIMARY instead".
* Replace deprecated "addWikiText".

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2021 First Faculty of Medicine, Charles University

## Third party plugins

* [Noty - A jQuery Notification Plugin](https://github.com/needim/noty)