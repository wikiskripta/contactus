--
-- Table structure for table `contactus_projects`
--

CREATE TABLE IF NOT EXISTS `contactus_projects` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL DEFAULT '',
  `isrecent` tinyint(1) NOT NULL DEFAULT '1',
  `www` varchar(255) NOT NULL DEFAULT ''
);


