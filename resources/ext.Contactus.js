/**
 * Javascript for extension
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 * @file
 */

( function ( mw, $ ) {

	// Copy embed code to clipboard after clicking button with class ".btn"
	var clipboard = new Clipboard('.btn');
	clipboard.on('success', function(e) {
		//e.preventDefault();
		noty({text: mw.message("contactus-embed-copied").text(), timeout:2000, progressBar:true, type:'information', theme:'relax', layout:'topCenter' });
	});
	
	$('form').submit(function(e){
    	e.preventDefault(); 
	    return false;
	});
	
	// switch to edit mode
	$(".cedit").click(function(){

		var crow = $(this).parent().parent();	// element TR, který máme editovat
		crow.find("td").prop("contenteditable", true);
		crow.find(".cembed").prop("contenteditable", false);
		crow.find(".cprojects").prop("contenteditable", false);
		crow.css("background-color", "#ffffff");
		// schovej všechny ostatní editační tlačítka
		$(".cpremove").hide(); 
		$(".cpedit").hide();
		$(".cremove").hide();
		$(".cedit").hide();
		$("#cpinsert").hide();
		$("#cpireset").hide();
		$("#cinsert").hide();
		$("#cireset").hide();
				
		// vytvořit form na daném řádku tabulky	
		if( crow.find(".cphoto").length > 0 ) {
			cphoto = crow.find(".cphoto").attr("title");
			cphotosrc = crow.find(".cphoto img").attr("src");
			crow.find(".cphoto").html(cphoto);
			crow.find(".cphoto").attr("title", cphotosrc);
		}
		else cphoto = "";
		
		ctit1 = crow.find(".ctit1").text();		
		cname = crow.find(".cname").text();
		csurname = crow.find(".csurname").text();
		ctit2 = crow.find(".ctit2").text();
		cemail = crow.find(".cemail").text();
		cphone = crow.find(".cphone").text();
		cworkplace = crow.find(".cworkplace").text();
		cjobtitle = crow.find(".cjobtitle").text();
		cprojects = crow.find(".cprojects").html();
		cwww = crow.find(".cwww").text();
		cnote = crow.find(".cnote").text();
		
		// nabídka projektů
		crow.find(".cprojects div").show();
		crow.find(".cprojects input").show();
		
		// new buttons save, cancel, reset
		crow.find(".caction").prop("contenteditable", false);
		crow.find(".caction").append("<button id='csave'>" + mw.message("contactus-save").text()  +"</button> ");
		crow.find(".caction").append("<button id='ccancel'>" + mw.message("contactus-cancel").text()  +"</button> ");
		crow.find(".caction").append("<button id='creset'>Reset</button>");
		
		// save
		$("#csave").click(function(){

			if( crow.find(".cphoto").length > 0 ) {
				var cphoto = crow.find(".cphoto").text();
				cphoto = cphoto.replace(/Soubor:*|File:*|\[\[|\]\]/i,"");
			}
			else var cphoto="";
						 
			var cid = crow.attr("id").replace("contact_", "");
			var ctit1 = crow.find(".ctit1").text();
			var cname = crow.find(".cname").text();
			var csurname = crow.find(".csurname").text();
			var ctit2 = crow.find(".ctit2").text();
			var cemail = crow.find(".cemail").text();
			var cphone = crow.find(".cphone").text();
			var cworkplace = crow.find(".cworkplace").text();
			var cjobtitle = crow.find(".cjobtitle").text();
			var cwww = crow.find(".cwww").text();
			var cnote = crow.find(".cnote").text();
			var data = 'cid=' + cid + '&cphoto=' + cphoto + '&ctit1=' + ctit1 + '&cname=' + cname + '&csurname=' + csurname + '&ctit2=' + ctit2 + '&cworkplace=' + cworkplace + '&cjobtitle=' + cjobtitle;
			data += '&cwww=' + cwww;
			data += '&cnote=' + cnote;
			data += '&cphone=' + cphone;
			data += '&cemail=' + cemail;
			data += '&cphone=' + cphone;
			var cprojects = "";
			crow.find(".cprojects input:checked").each(function(index, element) {
				cprojects += $(this).val() + ",";
			});
			data += '&cprojects=' + cprojects.slice(0,-1);
			data += '&caction=cupdate';

			// validation
			if( cname.length == 0 || csurname.length == 0 ) {
				noty({text: mw.message("contactus-validation-error").text(), timeout:4000, progressBar:true, type:'error', theme:'relax', layout:'topCenter' });
				return true;
			}	

			$.ajax({
				type: 'POST',
				url: 'http://' + window.location.hostname + '/index.php?title=Special:Contactus',
				data:  data,
				dataType: 'text',
				success: function( server_response ) {
					if(server_response == "success") {
						noty({text: mw.message("contactus-success").text(), type: 'success', timeout:2000, progressBar:true, theme:'relax', layout:'topCenter'});
						// úklid
						$("#csave").off("click");
						$("#ccancel").off("click");
						$("#creset").off("click");
						$("#csave").remove();
						$("#ccancel").remove();
						$("#creset").remove();
						window.location.reload();
						return true;
					}
					else alert(server_response);
				}
			});

		});
		
		// cancel
		$("#ccancel").click(function(){
			$("#csave").off("click");
			$("#ccancel").off("click");
			$("#creset").off("click");
			$("#csave").remove();
			$("#ccancel").remove();
			$("#creset").remove();
			window.location.reload();
			return true;
		});
		
		// reset
		$("#creset").click(function(){
			crow.find(".cphoto").html(cphoto);
			crow.find(".ctit1").html(ctit1);
			crow.find(".cname").html(cname);
			crow.find(".csurname").html(csurname);
			crow.find(".ctit2").html(ctit2);
			crow.find(".cemail").html(cemail);
			crow.find(".cphone").html(cphone);
			crow.find(".cworkplace").html(cworkplace);
			crow.find(".cjobtitle").html(cjobtitle);
			crow.find(".cprojects").html(cprojects);
			crow.find(".cprojects div").show();
			crow.find(".cprojects input").show();
			crow.find(".cwww").html(cwww);
			crow.find(".cnote").html(cnote);
		});

	});

	// remove row
	$(".cremove").click(function(){
		var crow = $(this).parent().parent();
		//$(this).parent().append("<div id='notyrowcont'></div>");

		//$("#notyrowcont").noty({
		noty({
			text: mw.message("contactus-remove-confirm").text(),
			theme:'relax', 
			layout:'topCenter',
			buttons: [
				{addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
						// this = button element
						// $noty = $noty element
						var cid = crow.attr("id").replace("contact_", "");
						$.ajax({
							type: 'POST',
							url: 'http://' + window.location.hostname + '/index.php?title=Special:Contactus',
							data:  'cid=' + cid + '&caction=cremove',
							dataType: 'text',
							success: function( server_response ) {
								if(server_response == "success") {
									$noty.close();
									noty({text: mw.message("contactus-removed").text(), type: 'success', timeout:2000, progressBar:true, theme:'relax', layout:'topCenter'});
									crow.remove();
								}
								else {
									$noty.close();
									server_response = server_response.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
									noty({text: server_response, type: 'error', timeout:10000, progressBar:true, theme:'relax', layout:'topCenter'});
									//alert(server_response);
								}
							}
						});
					}
				},
				{addClass: 'btn btn-danger', text: mw.message("contactus-cancel").text(), onClick: function($noty) {
						$noty.close();
					}
				}
			],
		});

	});	

	// onclick for inserting new row
	$("#cinsert").click(function(){
		
		var cform = $("#cinsertform");
				
		if( cform.find(".cphoto").length > 0 ) {
			var cphoto = cform.find(".cphoto input").val();
			cphoto = cphoto.replace(/Soubor:*|File:*|\[\[|\]\]/i,"");
		}
		else var cphoto="";
		
		var cid = 0;
		var ctit1 = cform.find(".ctit1 input").val();
		var cname = cform.find(".cname input").val();
		var csurname = cform.find(".csurname input").val();
		var ctit2 = cform.find(".ctit2 input").val();
		var cemail = cform.find(".cemail input").val();
		var cphone = cform.find(".cphone input").val();
		var cworkplace = cform.find(".cworkplace input").val();
		var cjobtitle = cform.find(".cjobtitle input").val();
		//var cprojects = cform.find(".cprojects input").val();
		var cwww = cform.find(".cwww input").val();
		var cnote = cform.find(".cnote input").val();
		var data = 'cid=' + cid + '&cphoto=' + cphoto + '&ctit1=' + ctit1 + '&cname=' + cname + '&csurname=' + csurname + '&ctit2=' + ctit2 + '&cworkplace=' + cworkplace + '&cjobtitle=' + cjobtitle + '&cwww=' + cwww + '&cnote=' + cnote;
		data += '&cemail=' + cemail + '&cphone=' + cphone + '&caction=cinsert';
		//data + ='&cprojects=' + cprojects;
		
		// validation
		if( cname.length == 0 ) {
			noty({text: mw.message("contactus-validation-error").text(), timeout:4000, progressBar:true, type:'error', theme:'relax', layout:'topCenter' });
			return true;
		}

		$.ajax({
			type: 'POST',
			url: 'http://' + window.location.hostname + '/index.php?title=Special:Contactus',
			data:  data,
			dataType: 'text',
			success: function( server_response ) {
				if(server_response == "success") {
					noty({text: mw.message("contactus-success").text(), timeout:2000, progressBar:true, type:'success', theme:'relax', layout:'topCenter' });
					window.location.reload();
					return true;
				}
				else alert(server_response);
			}
		});

	});

	// reset new row form
	$("#cireset").click(function(){
		var cform = $("#cinsertform");
		cform.find("input[type=text]").val("");
		cform.find("input[type=checkbox]").prop("checked", true);
	});










// PROJECTS
	// switch to edit mode
	$(".cpedit").click(function(){

		var crow = $(this).parent().parent();	// element TR, který máme editovat
		crow.find("td").prop("contenteditable", true);
		crow.find(".cpembed").prop("contenteditable", false);
		crow.css("background-color", "#ffffff");
		// schovej všechny ostatní editační tlačítka
		$(".cpremove").hide(); 
		$(".cpedit").hide();
		$(".cremove").hide();
		$(".cedit").hide();
		$("#cpinsert").hide();
		$("#cpireset").hide();
		$("#cinsert").hide();
		$("#cireset").hide();
		cpname = crow.find(".cpname").text();
		cpplace = crow.find(".cpplace").text();
		cpisrecent = crow.find(".cpisrecent input").prop("checked");
		crow.find(".cpisrecent input").prop("disabled", false);
		crow.find(".cpisrecent").prop("contenteditable", false);
		if(cpisrecent==true) cpisrecent = '1'; else cpisrecent = '0';	
		cpwww = crow.find(".cpwww").text();

		// new buttons save, cancel, reset
		crow.find(".cpaction").prop("contenteditable", false);
		crow.find(".cpaction").append("<button id='cpsave'>" + mw.message("contactus-save").text()  +"</button> ");
		crow.find(".cpaction").append("<button id='cpcancel'>" + mw.message("contactus-cancel").text()  +"</button> ");
		crow.find(".cpaction").append("<button id='cpreset'>Reset</button>");
		
		
		// save
		$("#cpsave").click(function(){

			var cpid = crow.attr("id").replace("project_", "");
			var cpname = crow.find(".cpname").text();
			var cpplace = crow.find(".cpplace").text();
			var cpisrecent = crow.find(".cpisrecent input").prop("checked");
			if(cpisrecent==true) cpisrecent = '1'; else cpisrecent = '0';
			var cpwww = crow.find(".cpwww").text();
			var data = 'cpid=' + cpid + '&cpname=' + cpname + '&cpwww=' + cpwww + '&cpplace=' + cpplace + '&cpisrecent=' + cpisrecent + '&caction=pupdate';

			// validation
			if( cpname.length == 0 ) {
				noty({text: mw.message("contactus-project-validation-error").text(), timeout:4000, progressBar:true, type:'error', theme:'relax', layout:'topCenter' });
				return true;
			}	

			$.ajax({
				type: 'POST',
				url: 'http://' + window.location.hostname + '/index.php?title=Special:Contactus',
				data:  data,
				dataType: 'text',
				success: function( server_response ) {
					if(server_response == "success") {
						noty({text: mw.message("contactus-success").text(), type: 'success', timeout:2000, progressBar:true, theme:'relax', layout:'topCenter'});
						// úklid
						$("#cpsave").off("click");
						$("#cpcancel").off("click");
						$("#cpreset").off("click");
						$("#cpsave").remove();
						$("#cpcancel").remove();
						$("#cpreset").remove();
						window.location.reload();
						return true;
					}
					else alert(server_response);
				}
			});
		});
		
		// cancel
		$("#cpcancel").click(function(){
			$("#cpsave").off("click");
			$("#cpcancel").off("click");
			$("#cpreset").off("click");
			$("#cpsave").remove();
			$("#cpcancel").remove();
			$("#cpreset").remove();
			window.location.reload();
			return true;
		});
		
		// reset
		$("#cpreset").click(function(){
			crow.find(".cpname").text(cpname);
			crow.find(".cpplace").text(cpplace);
			crow.find(".cpisrecent").prop("checked", cpisrecent);
			crow.find(".cpwww").text(cpwww);
		});

	});

	// remove row
	$(".cpremove").click(function(){
		var crow = $(this).parent().parent();

		noty({
			text: mw.message("contactus-remove-project-confirm").text(),
			theme:'relax', 
			layout:'topCenter',
			buttons: [
				{addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
						// this = button element
						// $noty = $noty element
						var cpid = crow.attr("id").replace("project_", "");
						$.ajax({
							type: 'POST',
							url: 'http://' + window.location.hostname + '/index.php?title=Special:Contactus',
							data:  'cpid=' + cpid + '&caction=premove',
							dataType: 'text',
							success: function( server_response ) {
								if(server_response == "success") {
									$noty.close();
									noty({text: mw.message("contactus-removed-project").text(), type: 'success', timeout:2000, progressBar:true, theme:'relax', layout:'topCenter'});
									//crow.remove();
									window.location.reload();
									return true;
								}
								else if(server_response == "err-related-contacts" ) {
									$noty.close();
									noty({text: mw.message("contactus-err-related-contacts").text(), type: 'warning', timeout:4000, progressBar:true, theme:'relax', layout:'topCenter'});
								}
								else {
									$noty.close();
									server_response = server_response.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
									noty({text: server_response, type: 'error', timeout:10000, progressBar:true, theme:'relax', layout:'topCenter'});
								}
							}
						});
					}
				},
				{addClass: 'btn btn-danger', text: mw.message("contactus-cancel").text(), onClick: function($noty) {
						$noty.close();
					}
				}
			],
		});

	});	

	// onclick for inserting new row
	$("#cpinsert").click(function(){
		var crow = $(this).parent().parent();
		var cpid = 0;
		var cpname = crow.find(".cpname input").val();
		var cpplace = crow.find(".cpplace input").val();
		var cpisrecent = crow.find(".cpisrecent input").prop("checked");
		if(cpisrecent==true) cpisrecent = '1'; else cpisrecent = '0';
		var cpwww = crow.find(".cpwww input").val();
		var data = 'cpid=' + cpid + '&cpname=' + cpname + '&cpwww=' + cpwww + '&cpplace=' + cpplace + '&cpisrecent=' + cpisrecent + '&caction=pinsert';

		// validation
		if( cpname.length == 0 ) {
			noty({text: mw.message("contactus-project-validation-error").text(), timeout:4000, progressBar:true, type:'error', theme:'relax', layout:'topCenter' });
			return true;
		}

		$.ajax({
			type: 'POST',
			url: 'http://' + window.location.hostname + '/index.php?title=Special:Contactus',
			data:  data,
			dataType: 'text',
			success: function( server_response ) {
				if(server_response == "success") {
					noty({text: mw.message("contactus-success").text(), timeout:2000, progressBar:true, type:'success', theme:'relax', layout:'topCenter' });
					window.location.reload();
					return true;
				}
				else alert(server_response);
			}
		});

	});

	// reset new row form
	$("#cpireset").click(function(){
		var crow = $(this).parent().parent();
		crow.find("input[type=text]").val("");
		crow.find("input[type=checkbox]").prop("checked", true);
	});

}( mediaWiki, jQuery ) );