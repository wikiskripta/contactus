<?php

/**
 * SpecialPage for Contactus extension
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class Contactus extends SpecialPage {
	function __construct() {
		parent::__construct( 'Contactus' );
	}

	function execute($param) {

		global $wgContactusFieldsSpecial, $wgReadOnly, $wgServer;
		$out = $this->getOutput();
		$request = $this->getRequest();
		$user = $this->getUser();
		$usercanedit = false;
		if( $user->getId() && (in_array( "sysop", $user->getGroups()) || in_array( "bureaucrat", $user->getGroups()) || in_array( "editor", $user->getGroups()) ) && !$wgReadOnly ) {
			$usercanedit = true;
		}


		# ##################################################################
		# SAVE DATA TO DB - only "sysop", "bureaucrat", "editor" can do it
		# This is called from the form
		####################################################################
		
		$ret = "success";
		$caction = $request->getVal( 'caction', '' );
		
		if( !$usercanedit && !empty($caction) ) {
			$out->disable();
			header( 'Content-type: application/text; charset=utf-8' );
			echo "err-user-not-allowed";
			return true;
		}
		elseif( $caction == 'cupdate' || $caction=='cremove' || $caction=='cinsert' ) {
			$cid = $request->getInt( 'cid', 0 );
			$cphoto = $request->getVal( 'cphoto', '' );
			$ctit1 = $request->getVal( 'ctit1', '' );
			$cname = $request->getVal( 'cname', '' );
			$csurname = $request->getVal( 'csurname', '' );
			$ctit2 = $request->getVal( 'ctit2', '' );
			$cphone = $request->getVal( 'cphone', '' );
			$cemail = $request->getVal( 'cemail', '' );
			$cwww = $request->getVal( 'cwww', '' );
			$cworkplace = $request->getVal( 'cworkplace', '' );
			$cjobtitle = $request->getVal( 'cjobtitle', '' );
			$cprojects = $request->getVal( 'cprojects', 0 );
			$cnote = $request->getVal( 'cnote', '' );
		}
		elseif( $caction == 'pupdate' || $caction=='premove' || $caction=='pinsert' ) {
			$cpid = $request->getInt( 'cpid', 0 );
			$cpname = $request->getVal( 'cpname', '' );
			$cpwww = $request->getVal( 'cpwww', '' );
			$cpplace = $request->getVal( 'cpplace', '' );
			$cpisrecent = $request->getVal( 'cpisrecent' );
			if( empty($cpisrecent) ) $cpisrecent = 0; else $cpisrecent = 1;
		}

		$conn = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancer();
		$dbw = $conn->getConnectionRef(DB_PRIMARY);
		switch($caction) {
			case 'cupdate':
				# UPDATE contact
				$update = array_merge( array('name' => $cname, 'surname' => $csurname ) );
				if( in_array( "degree", $wgContactusFieldsSpecial ) ) {
					$update = array_merge( $update, array('tit1' => $ctit1, 'tit2' => $ctit2 ) );
				}
				if( in_array( "email", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('email' => $cemail) );
				if( in_array( "phone", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('phone' => $cphone) );
				if( in_array( "workplace", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('workplace' => $cworkplace) );
				if( in_array( "jobtitle", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('job_title' => $cjobtitle) );
				if( in_array( "www", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('www' => $cwww) );
				if( in_array( "note", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('note' => $cnote) );
				if( in_array( "photo", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('photo' => $cphoto) );
				if( in_array( "projects", $wgContactusFieldsSpecial ) ) $update = array_merge( $update, array('projects' => $cprojects) );
								
				$res = $dbw->update(
					'contactus',
					$update,
					array( 'id' => $cid )
				);
				if ( !$res ) $ret = "err-update";
			break;

			case 'cremove':
				# REMOVE contact
				$res = $dbw->delete(
					'contactus',
					array( 'id' => $cid )
				);
				if ( !$res ) $ret = "err-delete";
			break;

			case 'cinsert':
				# INSERT contact
				$res = $dbw->insert(
					'contactus',
					array( 'tit1' => $ctit1, 'name' => $cname, 'surname' => $csurname, 'tit2' => $ctit2, 'email' => $cemail, 'phone' => $cphone, 'workplace' => $cworkplace,
						'job_title' => $cjobtitle, 'www' => $cwww, 'note' => $cnote, 'photo' => $cphoto, 'projects' => $cprojects )
				);
				if ( !$res ) $ret = "err-insert";
			break;

			case 'pupdate':
				# UPDATE project
				$res = $dbw->update(
					'contactus_projects',
					array( 'name' => $cpname, 'www' => $cpwww, 'place' => $cpplace, 'isrecent' => $cpisrecent ),
					array( 'id' => $cpid )
				);
				if ( !$res ) $ret = "err-update-project";
			break;

			case 'premove':
				# REMOVE project
				// move contacts from project first
				$res = $dbw->select(
					'contactus',
					array( 'id', 'projects' )
				);
				$relatedContacts = false;
				foreach ( $res as $row ) {
					if( preg_match( "/^".$cpid."$/", $row->projects ) || preg_match( "/^".$cpid.",/", $row->projects ) || preg_match( "/,".$cpid."$/", $row->projects ) || preg_match( "/,".$cpid.",/", $row->projects )) {
						$relatedContacts = true;
						$ret = "err-related-contacts";
						break;
					}
				}

				if( !$relatedContacts ) {
					$res = $dbw->delete(
						'contactus_projects',
						array( 'id' => $cpid )
					);
					if ( !$res ) $ret = "err-delete-project";
				}
			break;

			case 'pinsert':
				# INSERT project
				$res = $dbw->insert(
					'contactus_projects',
					array( 'name' => $cpname, 'www' => $cpwww, 'place' => $cpplace, 'isrecent' => $cpisrecent )
				);
				if ( !$res ) $ret = "err-insert-project";
			break;
		}

		if( !empty($caction) ) {
			## purge pages with inserted contacts ##
			// get namespaces
			$json = file_get_contents( "$wgServer/api.php?action=query&meta=siteinfo&siprop=namespaces&format=json" );
			$json = json_decode($json);
			$searchquery = "$wgServer/api.php?action=query&list=search&srsearch=contactus:&srwhat=text&format=json&srlimit=10&srnamespace=0";
			foreach( $json->query->namespaces as $ns ) {
				if( $ns->id > 0 && $ns->id < 200 ) $searchquery .= "|" . $ns->id;
			}
			// search and purge
			$continue = "&continue=";
			while( $continue ) {			
				$json = file_get_contents( $searchquery . $continue );
				$json = json_decode($json);
				if( $json->query->searchinfo->totalhits == 0 ) break;
				foreach( $json->query->search as $search ) {
					$ch = curl_init( "$wgServer/api.php" ); 
					curl_setopt( $ch, CURLOPT_POST, true); 
					curl_setopt( $ch, CURLOPT_POSTFIELDS, "action=purge&titles=" . $search->title ); 
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_exec($ch);
					curl_close($ch);
				}
				if( isset($json->continue) ) $continue = "&continue=" . $json->continue->continue . "&sroffset=" . $json->continue->sroffset;
				else $continue = "";
			}

			//$lb = wfGetLBFactory();
			//$lb->shutdown();
			$out->disable();
			header( 'Content-type: application/text; charset=utf-8' );
			echo $ret;
			return true;
		}

		# ##################################
		# SHOW SPECIAL PAGE
		####################################

		$this->setHeaders();
		$out->mBodytext = $this->msg( 'contactus-desc' )->plain();
		if( !$usercanedit ) $out->mBodytext = $this->msg( 'contactus-info-annon' )->text();
		
		$dbr = $conn->getConnectionRef(DB_REPLICA);

		## Contacts ##
		$out->addHTML( "<div style='overflow-x:auto;'>" );
		$out->addHTML( "<form id='ceditform'><table class='wikitable sortable contactusList'><tr>" );
		$out->addHTML( "<th class='unsortable'>" . $this->msg( 'contactus-embed' )->text() . "</th>" );
		if( in_array( "photo", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th class='unsortable'>IMG</th>" );
		if( in_array( "degree", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th>" . $this->msg( 'contactus-tit' )->text() . "</th>" );
		$out->addHTML( "<th>" . $this->msg( 'contactus-name' )->text() . "</th>" );
		$out->addHTML( "<th>" . $this->msg( 'contactus-surname' )->text() . "</th>" );
		if( in_array( "degree", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th>" . $this->msg( 'contactus-tit' )->text() . "</th>" );
		if( in_array( "email", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th>Email</th>" );
		if( in_array( "phone", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th class='unsortable'>" . $this->msg( 'contactus-phone' )->text() . "</th>" );
		if( in_array( "workplace", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th>" . $this->msg( 'contactus-workplace' )->text() . "</th>" );
		if( in_array( "jobtitle", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th>" . $this->msg( 'contactus-jobtitle' )->text() . "</th>" );
		if( in_array( "projects", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th>" . $this->msg( 'contactus-projects' )->text() . "</th>" );
		if( in_array( "www", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th>" . $this->msg( 'contactus-www' )->text() . "</th>" );
		if( in_array( "note", $wgContactusFieldsSpecial ) ) $out->addHTML( "<th class='unsortable'>" . $this->msg( 'contactus-note' )->text() . "</th>" );
		if( $usercanedit ) $out->addHTML( "<th class='unsortable'></th>" );
		$out->addHTML( "</tr>" );
	
		$res = $dbr->select(
			'contactus',
			array( 'id', 'tit1', 'name', 'surname', 'tit2', 'email', 'phone', 'workplace', 'job_title', 'projects', 'www', 'note', 'photo' ),
			array(),
			__METHOD__,
			array( 'ORDER BY' => 'surname' )
		);

		foreach ( $res as $row ) {
			$out->addHTML( "<tr id='contact_". $row->id ."'>" );
			$out->addHTML( "<td class='cembed'><button class='btn' data-clipboard-text='{{#contactus:". $row->id ."}}'>{{#contactus:". $row->id ."}}</button></td>" );
			if( in_array( "photo", $wgContactusFieldsSpecial ) ) {
				if( !empty($row->photo) ) {
					$out->addHTML( "<td class='cphoto' title='". $row->photo ."'>" );
					$out->addWikiTextAsInterface( "[[File:" . $row->photo . "|25px]]" );
				}
				else {
					$out->addHTML( "<td class='cphoto' title=''>" );
				}
			}
			$out->addHTML( "</td>" );
			if( in_array( "degree", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='ctit1'>". $row->tit1 ."</td>" );
			$out->addHTML( "<td class='cname'>". $row->name ."</td>" );
			$out->addHTML( "<td class='csurname'>". $row->surname ."</td>" );
			if( in_array( "degree", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='ctit2'>". $row->tit2 ."</td>" );
			if( in_array( "email", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='cemail'>". $row->email ."</td>" );
			if( in_array( "phone", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='cphone'>". $row->phone ."</td>" );
			if( in_array( "workplace", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='cworkplace'>". $row->workplace ."</td>" );
			if( in_array( "jobtitle", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='cjobtitle'>". $row->job_title ."</td>" );
			if( in_array( "projects", $wgContactusFieldsSpecial ) ) {
				$out->addHTML( "<td class='cprojects'>" );
				$res2 = $dbr->select(
					'contactus_projects',
					array( 'id', 'name', 'place', 'isrecent' ),
					array(),
					__METHOD__,
					array( 'ORDER BY' => 'name' )
				);
				if($res2) {
					$arr = explode( ",", $row->projects );
					foreach( $res2 as $row2 ) {
						$title = "";
						$recentstyle = 'color:red;';
						if( $row2->isrecent ) {
							$title .= $this->msg( 'contactus-isrecent' )->text();
							if( $row2->place ) $title .= ", ";
							$recentstyle = "color:green;";
						}
						if( $row2->place ) $title .= $row2->place;
						if( in_array( $row2->id, $arr ) ) {
							$out->addHTML( "<div title='$title' style='$recentstyle'><input type='checkbox' value='" . $row2->id . "' checked style='display:none;margin-left:0px;'> " . $row2->name . "</div>" );
						}
						else {
							$out->addHTML( "<div title='$title' style='display:none;$recentstyle'><input type='checkbox' value='" . $row2->id . "' style='display:none;margin-left:0px;'> " . $row2->name . "</div>" );
						}
					}
				}
				$out->addHTML( "</td>" );
			}
			if( in_array( "www", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='cwww'>". $row->www ."</td>" );
			if( in_array( "note", $wgContactusFieldsSpecial ) ) $out->addHTML( "<td class='cnote'>". $row->note ."</td>" );
			if( $usercanedit ) {
				$out->addHTML( "<td class='caction'><img src='$wgServer/extensions/Contactus/img/edit.png' alt='". $this->msg( 'contactus-edit' )->text() ."' title='". $this->msg( 'contactus-edit' )->text() ."' class='cedit'/> " );
				$out->addHTML( "<img src='$wgServer/extensions/Contactus/img/remove.png' alt='". $this->msg( 'contactus-remove' )->text() ."' title='". $this->msg( 'contactus-remove' )->text() ."' class='cremove'/></td>" );
			}
			$out->addHTML( "</tr>" );
		}
		$out->addHTML( "</table></form>" );
		$out->addHTML( "</div>" );

		$out->addHTML( "<div style='overflow-x:auto;'>" );
		if( $usercanedit ) {
			// new contact
			$out->addHTML( "<br><form id='cinsertform'><fieldset><legend>" . $this->msg( 'contactus-new' )->text() . ":</legend>" );
			if( in_array( "photo", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cphoto' title=''><input type='text' value='' name='cphoto' placeholder='" . $this->msg( 'contactus-photo-help' )->text() . "'></div>" );
			if( in_array( "degree", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='ctit1'><input type='text' value='' name='ctit1' placeholder='" . $this->msg( 'contactus-tit' )->text() . "'></div>" );
			$out->addHTML( "<div class='cname'><input type='text' value='' name='cname' autofocus placeholder='" . $this->msg( 'contactus-name' )->text() . "'></div>" );
			$out->addHTML( "<div class='csurname'><input type='text' value='' name='csurname' placeholder='" . $this->msg( 'contactus-surname' )->text() . "'></div>" );
			if( in_array( "degree", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='ctit2'><input type='text' value='' name='ctit2' placeholder='" . $this->msg( 'contactus-tit' )->text() . "'></div>" );
			if( in_array( "email", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cemail'><input type='text' value='' name='cemail' placeholder='Email'></div>" );
			if( in_array( "phone", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cphone'><input type='text' value='' name='cphone' placeholder='" . $this->msg( 'contactus-phone' )->text() . "'></div>" );
			if( in_array( "workplace", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cworkplace'><input type='text' value='' name='cworkplace' placeholder='" . $this->msg( 'contactus-workplace' )->text() . "'></div>" );
			if( in_array( "jobtitle", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cjobtitle'><input type='text' value='' name='cjobtitle' placeholder='" . $this->msg( 'contactus-jobtitle' )->text() . "'></div>" );
			if( in_array( "place", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cplace'><input type='text' value='' name='cplace' placeholder='" . $this->msg( 'contactus-place' )->text() . "'></div>" );
			//if( in_array( "projects", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cprojects'><input type='text' value='' name='cprojects' placeholder='" . $this->msg( 'contactus-projects' )->text() . "'></div>" );
			if( in_array( "www", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cwww'><input type='text' value='' name='cwww' placeholder='" . $this->msg( 'contactus-www' )->text() . "'></div>" );
			if( in_array( "note", $wgContactusFieldsSpecial ) ) $out->addHTML( "<div class='cnote'><input type='text' value='' name='cnote' placeholder='" . $this->msg( 'contactus-note' )->text() . "'></div>" );
			$out->addHTML( "<div class='caction'><img src='$wgServer/extensions/Contactus/img/save.png' alt='". $this->msg( 'contactus-new' )->text() ."' title='". $this->msg( 'contactus-new' )->text() ."' id='cinsert'/> " );
			$out->addHTML( "<img src='$wgServer/extensions/Contactus/img/remove.png' alt='Reset' title='Reset' id='cireset'/></div>" );
			$out->addHTML( "</fieldset></form>" );
		}

		## Projects ##
		$out->addHTML( "<form id='cprojectsform'><table class='wikitable sortable contactusList'><tr>" );
		$out->addHTML( "<th class='unsortable'>" . $this->msg( 'contactus-embed' )->text() . "</th>" );
		$out->addHTML( "<th>" . $this->msg( 'contactus-project-name' )->text() . "</th>" );
		$out->addHTML( "<th>" . $this->msg( 'contactus-place' )->text() . "</th>" );
		$out->addHTML( "<th class='unsortable'>" . $this->msg( 'contactus-isrecent' )->text() . "</th>" );
		$out->addHTML( "<th class='unsortable'>" . $this->msg( 'contactus-www' )->text() . "</th>" );
		if( $usercanedit ) $out->addHTML( "<th class='unsortable'></th>" );
		$out->addHTML( "</tr>" );

		$res = $dbr->select(
			'contactus_projects',
			array( 'id', 'name', 'place', 'isrecent', 'www' ),
			array(),
			__METHOD__,
			array( 'ORDER BY' => 'name' )
		);

		foreach ( $res as $row ) {
			$out->addHTML( "<tr id='project_". $row->id ."'>" );
			$out->addHTML( "<td class='cpembed'><button class='btn' data-clipboard-text='{{#contactus:". $row->id ."|project}}'>cc</button></td>" );
			$out->addHTML( "<td class='cpname'>". $row->name ."</td>" );
			$out->addHTML( "<td class='cpplace'>". $row->place ."</td>" );
			if( $row->isrecent ) $tmp = "<input type='checkbox' value='1' checked disabled>"; else $tmp = "<input type='checkbox' value='1' disabled>";
			$out->addHTML( "<td class='cpisrecent'>$tmp</td>" );
			$out->addHTML( "<td class='cpwww'>". $row->www ."</td>" );
			if( $usercanedit ) {
				$out->addHTML( "<td class='cpaction'><img src='$wgServer/extensions/Contactus/img/edit.png' alt='". $this->msg( 'contactus-edit' )->text() ."' title='". $this->msg( 'contactus-edit' )->text() ."' class='cpedit'/> " );
				$out->addHTML( "<img src='$wgServer/extensions/Contactus/img/remove.png' alt='". $this->msg( 'contactus-remove-project' )->text() ."' title='". $this->msg( 'contactus-remove-project' )->text() ."' class='cpremove'/></td>" );
			}
			$out->addHTML( "</tr>" );
		}

		// insert project
		$out->addHTML( "<tr id='newproject' class='sortbottom'>" );
		$out->addHTML( "<td></td>" );
		$out->addHTML( "<td class='cpname'><input type='text' value='' name='cpname' autofocus placeholder='" . $this->msg( 'contactus-project-name' )->text() . "'></td>" );
		$out->addHTML( "<td class='cpplace'><input type='text' value='' name='cpplace' placeholder='" . $this->msg( 'contactus-place' )->text() . "'></td>" );
		$out->addHTML( "<td class='cpisrecent'><input type='checkbox' value='' name='cpisrecent' value='1' checked></td>" );		
		$out->addHTML( "<td class='cpwww'><input type='text' value='' name='cpwww' placeholder='" . $this->msg( 'contactus-www' )->text() . "'></td>" );
		if( $usercanedit ) {
			$out->addHTML( "<td class='cpaction'><img src='$wgServer/extensions/Contactus/img/save.png' alt='". $this->msg( 'contactus-new-project' )->text() ."' title='". $this->msg( 'contactus-new-project' )->text() ."' id='cpinsert'/> " );
			$out->addHTML( "<img src='$wgServer/extensions/Contactus/img/remove.png' alt='Reset' title='Reset' id='cpireset'/></td>" );
		}
		$out->addHTML( "</tr>" );
		$out->addHTML( "</table></fieldset></form>" );
		$out->addHTML( "</div>" );
		$out->addModules('ext.Contactus');
		//$lb = wfGetLBFactory();
		//$lb->shutdown();
		return true;
	}		
}